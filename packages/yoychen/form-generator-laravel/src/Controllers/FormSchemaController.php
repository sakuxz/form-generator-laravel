<?php

namespace Yoychen\FormGeneratorLaravel\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yoychen\FormGeneratorLaravel\Models\FormSchema;

class FormSchemaController extends Controller
{
    public function index()
    {
        $formSchemas = FormSchema::all();

        return view('form_generator::formSchema.index', compact('formSchemas'));
    }

    public function create()
    {
        return view('form_generator::formSchema.create');
    }

    public function show($id)
    {
        $formSchema = FormSchema::findOrFail($id);
        
        return $formSchema;
    }

    public function store(Request $request)
    {
        $formSchema = FormSchema::create([
            'title' => request('title'),
            'description' => request('description'),
            'fieldset_schemas' => request('fieldset_schemas') ?? 'null',
        ]);

        return redirect(asset("/form-generator/?id=$formSchema->id&edit=true"));
    }

    public function update(Request $request, $id)
    {
        $formSchema = FormSchema::findOrFail($id);
        $formSchema->update($request->all());

        return $formSchema;
    }
}
