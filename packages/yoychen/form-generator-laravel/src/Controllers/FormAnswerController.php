<?php

namespace Yoychen\FormGeneratorLaravel\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yoychen\FormGeneratorLaravel\Models\FormAnswer;
use Yoychen\FormGeneratorLaravel\Models\FormSchema;
use Illuminate\Validation\Rule;

class FormAnswerController extends Controller
{
    public function show($formSchemaId)
    {
        $formAnswer = FormAnswer::where([
            'form_schema_id' => $formSchemaId,
            'user_id' => auth()->user()->id,
        ])->first();
        
        return $formAnswer;
    }

    public function update(Request $request, $formSchemaId)
    {
        $answer = $this->parseJsonToCollection(request('answer'), true);
        $fieldsetSchemas = $this->parseJsonToCollection(FormSchema::findOrFail($formSchemaId)->fieldset_schemas);

        $answerValidator = $this->getAnswerValidator($fieldsetSchemas);
        $errorMessages = $this->validateAnswer($answer, $answerValidator);

        if (count($errorMessages) > 0) {
            return response()->json($errorMessages, 412);
        }

        $formAnswer = FormAnswer::firstOrNew([
            'form_schema_id' => $formSchemaId,
            'user_id' => auth()->user()->id,
        ]);

        $formAnswer->answer = request('answer');
        $formAnswer->save();

        return $formAnswer;
    }

    protected function parseJsonToCollection(string $json)
    {
        return collect(json_decode($json, true));
    }

    protected function validateAnswer($answer, $answerValidator)
    {
        $errorMessages = [];
        $answerValidator->each(function ($fieldsetValidator, $uuid) use ($answer, &$errorMessages) {
            $ans = collect($answer[$uuid]['ans']);
            if ($ans->count() > $fieldsetValidator['multiple'] || $ans->count() === 0) {
                throw new \OutOfRangeException("Count of answers of $uuid is invalid.");
            }
            collect($answer[$uuid]['ans'])->each(function ($ans) use ($fieldsetValidator, &$errorMessages) {
                $validator = \Validator::make($ans, $fieldsetValidator['rules']);
                $errorMessages = array_merge(
                    $errorMessages,
                    array_flatten(
                        $this->parseMessageBagToErrorMessages(
                            $validator->messages(),
                            $fieldsetValidator['uuidToLabelMap']
                        )
                    )
                );
            });
        });
        
        return $errorMessages;
    }

    protected function parseMessageBagToErrorMessages($messageBag, $uuidToLabelMap)
    {
        return collect($uuidToLabelMap)->map(function ($label, $uuid) use ($messageBag) {
            return collect($messageBag->get($uuid))->map(function ($message) use ($label, $uuid) {
                return str_replace_first($uuid, $label, $message);
            });
        });
    }

    protected function getAnswerValidator($fieldsetSchemas)
    {
        $answerValidator = [];
        $fieldsetSchemas->each(function ($fieldsetSchema) use (&$answerValidator) {
            $rules = $this->getValidatorRulesFromFieldSchemas($fieldsetSchema['fieldSchemas']);
            $uuidToLabelMap = $this->getUuidToLabelMapFromFieldSchemas($fieldsetSchema['fieldSchemas']);
            $answerValidator[$fieldsetSchema['uuid']] = $this->getFieldsetValidator(
                $rules,
                $fieldsetSchema['multiple'] === false ? 1 : $fieldsetSchema['maxInput'],
                $uuidToLabelMap
            );
        });

        return collect($answerValidator);
    }

    protected function getFieldsetValidator(array $rules, int $multiple, array $uuidToLabelMap)
    {
        return [
            'rules' => $rules,
            'multiple' => $multiple,
            'uuidToLabelMap' => $uuidToLabelMap,
        ];
    }

    protected function getValidatorRulesFromFieldSchemas($fieldSchemas)
    {
        $rules = [];
        collect($fieldSchemas)->each(function ($fieldSchema) use (&$rules) {
            $rule = $this->getFieldRuleFromVeeValidateRule($fieldSchema['validationRules']);
            $rules[$fieldSchema['uuid']] = $rule;
        });

        return $rules;
    }

    protected function getUuidToLabelMapFromFieldSchemas($fieldSchemas)
    {
        $uuidToLabelMap = [];
        collect($fieldSchemas)->each(function ($fieldSchema) use (&$uuidToLabelMap) {
            $uuidToLabelMap[$fieldSchema['uuid']] = $fieldSchema['label'];
        });

        return $uuidToLabelMap;
    }

    protected function getFieldRuleFromVeeValidateRule($validator)
    {
        $rule = '';
        collect($validator)->each(function ($ruleParameters, $ruleName) use (&$rule) {
            $rule .= "$ruleName";
            if (count($ruleParameters) > 0) {
                $rule .= ':';
                foreach ($ruleParameters as $index => $ruleParameter) {
                    if ($index === 0) {
                        $rule .= $ruleParameter;
                    } else {
                        $rule .= ",$ruleParameter";
                    }
                }
            }
            $rule .= "|";
        });

        return $rule;
    }
}
