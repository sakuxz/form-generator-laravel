<?php

namespace Yoychen\FormGeneratorLaravel\Models;

use Illuminate\Database\Eloquent\Model;

class FormAnswer extends Model
{
    protected $guarded = [];
}
