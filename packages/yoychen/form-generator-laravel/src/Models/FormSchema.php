<?php

namespace Yoychen\FormGeneratorLaravel\Models;

use Illuminate\Database\Eloquent\Model;

class FormSchema extends Model
{
    protected $guarded = [];
}
