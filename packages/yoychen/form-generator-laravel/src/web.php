<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::resource('api/formSchema', 'Yoychen\FormGeneratorLaravel\Controllers\FormSchemaController')
        ->only(['update', 'show']);

    Route::resource('api/formAnswer', 'Yoychen\FormGeneratorLaravel\Controllers\FormAnswerController')
        ->only(['update', 'show']);
    
    Route::resource('formSchema', 'Yoychen\FormGeneratorLaravel\Controllers\FormSchemaController')
        ->only(['index', 'create', 'store']);
});
