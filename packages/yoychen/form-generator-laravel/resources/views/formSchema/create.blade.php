
@extends('form_generator::layout')

@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('formSchema.store') }}" method="POST">
                    <h1>建立表單</h1>
                    <hr>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                    @csrf
                    <button type="submit" class="btn btn-primary mt-2">建立</button>
                    <a href="{{ route('formSchema.index') }}" class="btn mt-2">返回</a>
                </form>
            </div>
        </div>
    </div>
@endsection
