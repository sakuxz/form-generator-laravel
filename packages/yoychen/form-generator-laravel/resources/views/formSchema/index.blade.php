
@extends('form_generator::layout')

@section('content')
    <div class="container mt-5">
        <div class="clearfix mb-3">
            <a href="{{ route('formSchema.create') }}" class="btn btn-primary float-right">建立新表單</a>
            <h1>表單列表</h1>
        </div>

        @foreach($formSchemas as $formSchema)
            <div class="list-group">
                <a href="{{ asset("/form-generator/?id=$formSchema->id") }}" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{ $formSchema->title }}</h5>
                        <small>{{ $formSchema->created_at }}</small>
                    </div>
                    <pre class="mb-1">{{ $formSchema->description }}</pre>
                </a>
            </div>
        @endforeach
    </div>
@endsection
